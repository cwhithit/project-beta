import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
const ManufacturerList = () => {
    const [manufacturers, setManufacturers] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }
    useEffect (() => {
        getData()
    }, [])
    return (
        <div>
            <h1>Vehicle Manufacturers</h1>
            <div className="d-grip gap-2 d-sm-flex justify-content-sm-right">
                <Link to="/manufacturers/new/" className="btn btn-primary btn-md px-4 gap-23">Add New Manufacturers</Link>
            </div>
                <table className="table table-striped table bordered table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {manufacturers.map(manufacturer => {
                            return (
                                <tr key={manufacturer.href}>
                                    <td>{manufacturer.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
        </div>
    )
}

export default ManufacturerList;
