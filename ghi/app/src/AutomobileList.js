import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
const AutomobileList = () => {
    const [autos, setAutos] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        }
    }
    useEffect(() => {
        fetchData()
    }, []);

    return (
        <div>
            <h1>Automobiles</h1>
            <div className="d-grip gap-2 d-sm-flex justify-content-sm-right">
                <Link to="/automobiles/new/" className="btn btn-primary btn-md px-4 gap-23">Add New Automobiles</Link>
            </div>
                <table className="table table-striped table bordered table-hover">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Model</th>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {autos.map(auto => {
                            return (
                                <tr key={auto.href}>
                                    <td>{auto.vin}</td>
                                    <td>{auto.color}</td>
                                    <td>{auto.year}</td>
                                    <td>{auto.model.name}</td>
                                    <td>{auto.model.manufacturer.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
        </div>
    )
}
    export default AutomobileList;
