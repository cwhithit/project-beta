import React, {useEffect, useState} from 'react';

function TechnicianForm() {

    const [name, setName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');

    const handleNameChange = (event) => {
        const value  = event.target.value;
        setName(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value  = event.target.value;
        setEmployeeNumber(value);
    }
    const handleSubmit= async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.employee_number = employeeNumber;

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type" : 'application/json',
            },
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setEmployeeNumber('');
        }
    }
    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Enter A Technician</h1>
                        <form id="create-manufacturer-form" onSubmit={handleSubmit}>
                            <div className="form-floating-mb-3">
                                <input className="form-control" value={name} onChange={handleNameChange} placeholder="Name" type="text" id="name" name="name" required />
                            </div>
                            <div className="form-floating-mb-3 mt-2">
                            <input className="form-control" value={employeeNumber} onChange={handleEmployeeNumberChange} placeholder="Employee Number" type="text" id="employee_number" name="employee_number" required />
                        </div>
                                <div>
                                    <button className="btn mt-2  btn-primary">Create</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default TechnicianForm;
