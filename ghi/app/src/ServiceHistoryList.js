import React, { useState, useEffect } from 'react';

const ServiceHistory = () => {
    const [appointments, setAppointments] = useState([]);
    const [filteredApps, setFilteredApps] = useState([]);
    const [searchVin, setSearchVin] = useState('');
    const [submitted, setSubmitted] = useState(false);


    const fetchData = async () => {
        const appointmentsUrl = 'http://localhost:8080/api/appointments/';
        const response = await fetch(appointmentsUrl);

        if(response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSearchVinChange = (event) => {
        setSearchVin(event.target.value)
    };


    const handleButtonChange = (event) => {
        let search = appointments.filter(appointment => {
            return(
                searchVin === appointment.vin
            )
        })
        setFilteredApps(search)
        setSubmitted(true);

    }

    return(
        <div>
            <div className="mt-3 mb-3">
                <input className="btn btn-outline-secondary me-3
                " type="text" placeholder="Enter VIN" maxLength={17} value={searchVin} id="form1" onChange={handleSearchVinChange} />
                <button className="btn btn-outline-success" type="button" onClick={handleButtonChange}>Search VIN</button>
            </div>
            <h1>Appointment Service History</h1>
                <table className="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Appointment Reason</th>
                            <th>Technician</th>
                            <th>VIP</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filteredApps.map(appointment => {
                            return(
                            <tr key = {appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer}</td>
                                <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                                <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.technician.name}</td>
                                <td>{appointment.is_vip ? "Yes" : "No"}</td>
                            </tr>
                            );
                        })}
                    </tbody>
                </table>
            {submitted && filteredApps.length === 0 && (
                <div className="alert alert-warning" role="alert">
                    <p>Invalid VIN :/</p>
                </div>
            )}
        </div>
    );
}
export default ServiceHistory;
