import React, {useEffect, useState} from 'react';

const AppointmentForm = () => {
    const [technicians, setTechnicians] = useState([]);



    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [technician, setTechnician] = useState('');
    const [complete, setComplete] = useState(false);

    const clearStates = () => {
        setVin('');
        setCustomer('');
        setDateTime('');
        setReason('');
        setTechnician('');
        setComplete(true);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = dateTime;
        data.reason = reason;
        data.technician = technician;



        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method : "post",
            body : JSON.stringify(data),
            headers : {
                "Content-Type" : "application/json",
            },
        };

        const appResponse = await fetch(appointmentUrl, fetchConfig);
            if (appResponse.ok) {
                const newAppointment = await appResponse.json();
                clearStates();
        }

    };

    const fetchData = async () => {
        const techniciansUrl = "http://localhost:8080/api/technicians/";

        const techniciansResponse = await fetch(techniciansUrl);


        if (techniciansResponse.ok) {
            const techniciansData = await techniciansResponse.json();

            setTechnicians(techniciansData.technicians);

        }
        };

    useEffect(() => {
        fetchData();
    }, []);



    return (
        <div className ="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>New Appointment</h1>
                    <form id="create-appointment-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input className="form-control" type="text" required id="vin" name="vin" placeholder="VIN" maxLength={17} onChange={handleVinChange} value={vin}/>
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" type="text" required id="customer" name="customer" placeholder="Vehicle Owner" onChange={handleCustomerChange} value={customer}/>
                            <label htmlFor="customer">Vehicle Owner</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" type="datetime-local" required id="date-time" name="date-time" placeholder="Date and Time" onChange={handleDateTimeChange} value={dateTime}/>
                        </div>
                        <div className="mb-3">
                            <textarea className="form-control" type="text" required id="reason" name="reason" placeholder="Appointment Reason" onChange={handleReasonChange} value={reason}></textarea>
                        </div>
                        <div className="mb-3">
                            <select className="form-select" required id="technician" name="technician" onChange={handleTechnicianChange} value={technician}>
                                <option value="">Select a Technician</option>
                                {technicians.map((technician) => {
                                    return (
                                        <option key={technician.name} value={technician.name}>
                                            {technician.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="col text-center">
                            <button className="btn btn-primary">Create Appointment</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};
export default AppointmentForm;
