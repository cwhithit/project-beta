import React, { useState, useEffect } from 'react';
import {Link} from 'react-router-dom';

const ModelList = () => {
  const [models, setModels] = useState([]);

  const fetchModels = async () => {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
      const models = data.models
      setModels(models)
    }
  }


  useEffect(() => {
      fetchModels();
  }, []);

    return (
      <div>
        <h1>Models</h1>
        <div className="d-grip gap-2 d-sm-flex justify-content-sm-right">
                <Link to="/models/new/" className="btn btn-primary btn-md px-4 gap-23">Add New Models</Link>
            </div>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map(model => {
            return (
              <tr key={model.id}>
                <td>{ model.name }</td>
                <td>{ model.manufacturer.name }</td>
                <td><img src={model.picture_url} className="img-thumbnail" alt="model_pic" width="200" height="200"></img> </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
}

export default ModelList;
