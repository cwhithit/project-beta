import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import AutomobileList from './AutomobileList';
import ModelList from './ModelList';
import CreateModelForm from './CreateModelForm'
import AutomobileForm from './AutomobileForm'
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import SalesForm from './SalesForm';
import SalesList from './SalesList';
import Salesperson from './Salesperson';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistoryList';
function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
              <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="models/">
            <Route index element={<ModelList />} />
            <Route path="new" element={<CreateModelForm />} />
          </Route>
          <Route path="salespersons/">
            <Route index element={<SalespersonForm  />} />
            <Route path="record" element={<Salesperson />} />
          </Route>
          <Route path="customer/" element={<CustomerForm  />} />
          <Route path="sales/">
            <Route index element={<SalesList  />} />
            <Route path="new" element={<SalesForm />} />
          </Route>
          <Route path="technicians/" element={<TechnicianForm />} />
          <Route path="appointments/">
            <Route index element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
