import React, {useState} from 'react';

const ManufacturerForm = () => {
    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value  = event.target.value;
        setName(value);
    }
    const handleSubmit= async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;

        const ManufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type" : 'application/json',
            },
        };
        const response = await fetch(ManufacturerUrl, fetchConfig);
        if (response.ok) {
            setName('')
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Manufacturer</h1>
                        <form id="create-manufacturer-form" onSubmit={handleSubmit}>
                            <div className="form-floating-mb-3">
                                <input className="form-control" value={name} onChange={handleNameChange} placeholder="Name" type="text" id="name" name="name" required />
                            </div>
                                <div>
                                    <button className="btn mt-2  btn-primary">Create</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ManufacturerForm;
