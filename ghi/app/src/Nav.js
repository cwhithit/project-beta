import { NavLink, Link } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <div className="nav-item dropdown">
              <NavLink className="btn btn-secondary dropdown-toggle bg-success me-1" to="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </NavLink>
              <ul className="dropdown-menu me-1" aria-labelledby="dropdownMenuLink">
                <li><Link className="dropdown-item" to="/manufacturers">Manufacturers</Link></li>
                <li><Link className="dropdown-item" to="/manufacturers/new">Create A New Manufacturer</Link></li>
                <li><Link className="dropdown-item" to="/models">Models</Link></li>
                <li><Link className="dropdown-item" to="/models/new">Create A New Model</Link></li>
                <li><Link className="dropdown-item" to="/automobiles">Automobiles</Link></li>
                <li><Link className="dropdown-item" to="/automobiles/new">Create A New Automobile</Link></li>
              </ul>
            </div>
            <div className="nav-item dropdown me-1">
              <NavLink className="btn btn-secondary dropdown-toggle bg-success" to="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </NavLink>
              <ul className="dropdown-menu me-1" aria-labelledby="dropdownMenuLink">
              <li><Link className="dropdown-item" to="/salespersons">Create New Sales Person</Link></li>
                <li><Link className="dropdown-item" to="/customer">New Customer Form</Link></li>
                <li><Link className="dropdown-item" to="/sales">Cars Sold!</Link></li>
                <li><Link className="dropdown-item" to="/sales/new">Sell Form</Link></li>
                <li><Link className="dropdown-item" to="/salespersons/record">Salesperson Records</Link></li>
              </ul>
            </div>
            <div className="nav-item dropdown">
              <NavLink className="btn btn-secondary dropdown-toggle bg-success" to="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </NavLink>
              <ul className="dropdown-menu me-1" aria-labelledby="dropdownMenuLink">
                <li><Link className="dropdown-item" to="/technicians">Enter A New Technician</Link></li>
                <li><Link className="dropdown-item" to="/appointments/new">Schedule A Service Appointment</Link></li>
                <li><Link className="dropdown-item" to="/appointments">List Of Appointments</Link></li>
                <li><Link className="dropdown-item" to="/appointments/history">Service Appointment History</Link></li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}
export default Nav;
