import React, {useState, useEffect} from 'react';

const AppointmentList= () => {
    const [appointments, setAppointments] = useState([]);

    const fetchAppointments = async () => {
        const appsUrl = "http://localhost:8080/api/appointments/";
        const appResponse = await fetch(appsUrl);

        if (appResponse.ok) {
            const appsData = await appResponse.json();
            setAppointments(appsData.appointments);
        }
    }
    useEffect(() => {
        fetchAppointments();
    }, []);

        const completeAppointment = async (pk) => {
            const completeAppUrl = `http://localhost:8080/api/appointments/${pk}/`;
            const fetchConfig = {
                method : "put",
                body : JSON.stringify({is_complete : true}),
                headers : {
                    "Content-Type" : "application/json",
                },
            };

            const completeResponse = await fetch(completeAppUrl, fetchConfig);
            if (completeResponse.ok) {
                fetchAppointments();
            }
        };


        const deleteAppointment = async (pk) => {
            const deleteAppUrl = `http://localhost:8080/api/appointments/${pk}/`;
            const fetchConfig = {
                method : "delete",
                headers : {
                    "Content-Type" : "application/json",
                },
            };

            const deleteResponse = await fetch(deleteAppUrl, fetchConfig);
            if (deleteResponse.ok) {
                fetchAppointments();
            }
        };


    return (
            <>
            <h1>Scheduled Appointments</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Reason</th>
                            <th>Technician</th>
                            <th>VIP</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map(appointment => {
                            if (!appointment.is_complete) {
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{new Date(appointment.date_time).toLocaleDateString('en-US')}</td>
                                        <td>{new Date(appointment.date_time).toLocaleTimeString([],{
                                            hour: "2-digit",
                                            minute: "2-digit",
                                        })}</td>
                                        <td>{appointment.technician.name}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.is_vip ? "Yes" : "No"}</td>
                                        <td>
                                            <button className="btn btn-secondary m-2" style= {{ backgroundColor : 'red'}} onClick={(e) => deleteAppointment(appointment.id)}>Cancel</button>
                                            <button className="btn btn-primary" style= {{ backgroundColor : 'green'}} onClick={(e) => completeAppointment(appointment.id)}>Complete</button>
                                        </td>

                                    </tr>
                                );
                            }

                        })}
                    </tbody>
                </table>
            </>
        );
    };
export default AppointmentList;
