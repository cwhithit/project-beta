# CarCar

Team:

- Daniel Ahdoot - Sales Mircoservice
- Chase Whitaker - Service Microservice

## Design

CarCar is a web application that is designed to track inventory, sales, and services for an auto dealership. Each application has its own microservice, utilizing RESTful API'S to provide each other with information. This project utilizes docker.

### CarCar Features:

- Create Auto Manufacturers and List Manufacturers
- Create Vehicle Models and List Models
- Create Automobiles and List Automobiles
- Create Sales Persons
- Create Potential Customers
- Create a Sale and List all Sales
- A list of a Salesperson's Sale Record/History
- Create Technicians
- Create, List, Delete and complete Service Appointments
- Service Appointment List page with VIP Feature for Vehicles in Inventory
- Search for Service History of a vehicle by the VIN

### Context Map:

![](images/Picture.png)

## Installation Guide

1. Fork Repository from https://gitlab.com/cwhithit/project-beta
2. Clone to your local machine with git clone (insert HTTPS)
3. While Docker is running, input following commands into terminal (be sure to wait for the blinking ucrsor to reappear before entering you next ):
4. `docker volume create beta-data`
5. `docker-compose build`
6. `docker-compose up`
7. Access site via http://localhost:3000
---
## How to Use:

### To create a vehicle:

A manufacturer is required to create a model and a model is required to create an automobile.

1. Begin by creating a manufacturer from the inventory dropdown.
2. Create a model from the inventory dropdown.
3. Create automobile from the inventory dropdown.
4. You can view the items you created in each of their respective lists.

### To sell vehicles:

A customer, salesperson and a vehicle are required to create a sale.

1. Begin by creating a customer from the sales dropdown.
2. Create a salesperson from the sales dropdown.
3. Sell a vehicle from the sales drop down.
4. Get a list of all vehicles sold from the Sales dropdown.
5. Get salesperson recrods from the sales dropdown.

### To schedule a service
A technician is required to schedule an appointment.
1. Begin by creating a new technician from the Service -> Add a Technician dropdown -> complete the form. (Note that like any other ID, the employee number/ID has to be unique, no repeating IDs)
2. Create a service appointment from the Service -> Add a Service Appointment dropdown -> complete the form.
3. To cancel or complete an active service appointment, Select Service -> Show a List of Appointments -> Cancel / Complete.
4. To see a list of all active and past completed service appointments for a specific VIN, select Service -> Service Appointment History -> Enter a VIN in the prompt and click 'Search'. (Cars that have not been serviced will not show up since they don't have any history)
---
## Inventory insomnia URL, Port, JSON requirements
## All of the Inventory Views RESTful API (PORT 8100)
### Manufacturers
| Method | URL | Purpose |
| ------ | ------ | ------ |
|   GET     |  `http://localhost:8100/api/manufacturers/`      | Lists all manufacturers       |
|   POST    |  `http://localhost:8100/api/manufacturers/`      |  Creates a new manufacturer, and adds it to the list      |
|   GET   |   `http://localhost:8100/api/manufacturers/<id>/`     | Shows a manufacturer's details       |
|   PUT    |  `http://localhost:8100/api/manufacturers/<id>/`      |  Updates the details of a manufacturer      |
|   DELETE   |  `http://localhost:8100/api/manufacturers/<id>/`      |  Deletes a manufacturer from the list      |
 
- Create/POST Manufacturer - http://localhost:8100/api/manufacturers/
- JSON Input for Create or Update:
```
{
  "name": "Toyota"
}
```
-JSON Output for Create, Update, or Individual Details:
```
{
	"href": "/api/manufacturers/5/",
	"id": 5,
	"name": "Toyota"
}

```
- List/GET Manufacturers - http://localhost:8100/api/manufacturers/
- JSON Output:
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Crysler"
		},
	]
}
```
## Vehicle Models
| Method | URL | Purpose |
| ------ | ------ | ------ |
|   GET     |  `http://localhost:8100/api/models/`      | Lists all vehicle models       |
|   POST    |  `http://localhost:8100/api/models/`      |  Creates a new vehicle model, and adds it to the list      |
|   GET   |   `http://localhost:8100/api/models/<id>/`     | Shows a vehicle model's details       |
|   PUT    |  `http://localhost:8100/api/models/<id>/`      |  Updates the details of a vehicle model      |
|   DELETE   |  `http://localhost:8100/models/<id>/`      |  Deletes a vehicle model from the list      |

- Create/POST Model - http://localhost:8100/api/models/
- JSON Input for Create or Update:

```
{
  "name": "Camry",
  "picture_url": "https://www.beavertontoyota.com/assets/stock/colormatched_01/transparent/1280/cc_2023toc02_01_1280/cc_2023toc020036_01_1280_1h1.png?height=400&bg-color=FFFFFF",
  "manufacturer_id": "5"
}
```
-JSON Output for Create, Update, or Individual Detials:
```
{
	"href": "/api/models/7/",
	"id": 7,
	"name": "Camry",
	"picture_url": "https://www.beavertontoyota.com/assets/stock/colormatched_01/transparent/1280/cc_2023toc02_01_1280/cc_2023toc020036_01_1280_1h1.png?height=400&bg-color=FFFFFF",
	"manufacturer": {
		"href": "/api/manufacturers/5/",
		"id": 5,
		"name": "Toyota"
	}
}
```
- List/GET Models - http://localhost:8100/api/models/
- JSON Output:
```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Car",
			"picture_url": "car",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Crysler"
			}
		}
	]
}
```
### Automobiles
| Method | URL | Purpose |
| ------ | ------ | ------ |
|   GET     |  `http://localhost:8100/api/automobiles/`      | Lists all automobiles       |
|   POST    |  `http://localhost:8100/api/automobiles/`      |  Creates a new automobile, and adds it to the list      |
|   GET   |   `http://localhost:8100/api/automobiles/<vin>/`    | Shows an automobile's details       |
|   PUT    |  `http://localhost:8100/api/automobiles/<vin>/`      |  Updates the details of an automobile      |
|   DELETE   |  `http://localhost:8100/automobiles/<vin>/`      |  Deletes an automobile from the list      |

- Create/POST Automobile - http://localhost:8100/api/automobiles/
- JSON Input for Create or Update:
```
{
  "color": "Red",
  "year": 2023,
  "vin": "204ghit89h34sar67",
  "model_id": 4
}
```
- JSON Output for Create, Update, or Individual Detials:
```
{
	"href": "/api/automobiles/204ghit89h34sar67/",
	"id": 12,
	"color": "Red",
	"year": 2023,
	"vin": "204ghit89h34sar67",
	"model": {
		"href": "/api/models/7/",
		"id": 7,
		"name": "Camry",
		"picture_url": "https://www.beavertontoyota.com/assets/stock/colormatched_01/transparent/1280/cc_2023toc02_01_1280/cc_2023toc020036_01_1280_1h1.png?height=400&bg-color=FFFFFF",
		"manufacturer": {
			"href": "/api/manufacturers/5/",
			"id": 5,
			"name": "Toyota"
		}
	}
}
```
- List/GET Automobiles - http://localhost:8100/api/automobiles/
- JSON Ouput:
```
{
	"autos": [
		{
			"href": "/api/automobiles/clskanvo/",
			"id": 1,
			"color": "aciabcljh",
			"year": 2000,
			"vin": "clskanvo",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Car",
				"picture_url": "car",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Crysler"
				}
			}
		},
	]
}
```
## Sales Microservice

### Models For The Sales Microservice:

- AutomobileVO
- Salesperson
- PotentialCustomer
- Sales

the sales model uses the AutmobileVO, Salesperson and PotentialCustomer models as foreign keys.

### Integration With Inventory Microservice:

The Sales microservice will poll data from the automobiles in inventory to an AutomobileVO model (value object). The AutomobileVO model has an added sold BoolianField that is set the False. When a vehicle is sold in the Sales form, the sold value will change to True. It will then be used to filter out sold cars from the saleform VIN dropdown. The Salesperson record and list of all Sales page will update when a car is sold to provide that data.

## Sales Insomnia URL, Port, JSON Requirements

### Salesperson:

- Create/POST Salesperson - http://localhost:8090/api/salespersons/
- JSON:

```
{
  "name": "Danny",
  "employee_number": "12345"
}
```

- List/GET Salesperson - http://localhost:8090/api/salespersons/

### Customer

- Create/POST Customer - http://localhost:8090/api/customer/
- JSON:

```
	{
			"name": "Some Name",
			"street": "Street address",
			"city": "A city",
			"zip_code": "Zip Code",
			"state": "State",
			"phone": "5551551555"
	}
```

### Sale

- Create/POST Sale - http://localhost:8090/api/sales/
- JSON:

```
{
  "sale_price": "18000",
  "automobile": "/api/automobiles/233CC5DB11/",
	"customer": "A Name",
	"salesperson": "A Name"
}
```

- List/GET Sales - http://localhost:8090/api/sales/

### Polling for Sales

- List/GET Automobile Value Objects - http://localhost:8090/api/automobiles/

CarCar is a web application that is designed to track a automobile dealership by tracking inventory, sales and services for dealerships. Each application is split into individual microservice utilizing RESTful API's to provide information to each other.The project utilizes docker.

---
# Service Microservice

## Overview
The service application allows users to manage and view data for automobile service appointments. Within the application, users are able to:
- Add new technicians to the technicians roster with the Technicians form
- Schedule new service appointments with the Appointments Form
- View a list of all active appointments
- Search up service appointment history by entering a vehilce VIN. 
	- If the vehicle was once stored in the Dealerships inventory, then the service history for the vehilce will be registered as a VIP.

## The Service Backend
The Service Microservice contians two applications, API and poll. The API microservice contians the back-end items for the services microservice to function properly. This part handles the creating, deleting, updating, and saving data for the service appointments and technicians. Contains the models, views, and urls responsible for handling the requests from the frontend.

## The Service Frontend
The Service Microservice frontend is where everything we did on the backend comes to life. Users can interact with the technician form to register new technicians or appointment form to register new appointments. Users cna also view the scheduled appointments, and mark them as "cancelled" to delete the appointment from the list, or "completed" to save the appointment in service history. We chose to delete the cancelled appointments, because as far as we see it, those appointments did not happen, so why save them? 

(However, if you don't like this feature and want to know who keeps cancelling on you, this can be changed to save cancelled appointments by going into the directory project-beta -> ghi -> app -> src -> AppointmentList.js: The Locate the `const deleteAppointment`, and change the word "delete" to "post" within that variable. Congrats you're now a software developer!)

The appointment list displays whether a car is a VIP by checking the VIN with the dealerships inventory history. If it was sold by the dealership it is a VIP.

## Models for the Service microservice:
### AutomobileVO (THIS IS THE VALUE OBJECT)
Properties:
- import_href
- vin
- color
- year 

### Technician
Properties:
- name
- employee_number

### Appointment
Properties:
- vin
- customer
- date_time
- reason 
-technician
This Service model uses the Technician model as a foreign key.
- is_vip
- is_complete 

### Integration With Inventory Microservice:

The Service microservice uses poller.py to poll data from the Inventory on an interval time of 1 minute, it polls data regarding automobile VINs to an AutomobileVO model (value object). This is how we check for VIPs.

### Polling for Service Appointments
- List/GET Automobile Value Objects - http://localhost:8080/api/automobileVOs/
---
## Service Appointment http RESTful APIs (PORT 8080)
## Insomnia URL, Port, JSON Requirements:
### Technician:

|	Method	|	URL	|	Purpose	|
| ---   | ---   | ---   |
|	GET	|	`http://localhost:8080/api/technicians/`	|	List Technicians	|
| POST	|	`http://localhost:8080/api/technicians/`	|  Create A New Technician |
| GET	|   `http://localhost:8080/api/technicians/<id>/` | Get Details of Individual Technician |
| PUT   |   `http://localhost:8080/api/technicians/<id>/` | Update Details of Individual Technician |
| DELETE|   `http://localhost:8080/api/technicians/<id>/` | Delete Details of Individual Technician |

- Create/POST Technician - http://localhost:8080/api/technicians/
- JSON Input for Create or Update:
```
{
  "name": "The Dude",
  "employee_number": "12"
}
```
- JSON Output for Create, Update, or Individual Details:
```
{
	"id": 1,
	"name": "Bob",
	"employee_number": "1"
}
```
- List/GET Technicians - http://localhost:8080/api/technicians/
- JSON Output:
```
{
	"technicians": [
		{
			"id": 1,
			"name": "Bob",
			"employee_number": "1"
		}
	]
}
```
### Service Appointments
|	Method	|	URL	|	Purpose	|
| ---   | ---   | ---   |
|	GET	|	`http://localhost:8080/api/appointment/`	|	List Appointments	|
| POST	|	`http://localhost:8080/api/appointments/`	|  Create A New Appointment |
| GET	|   `http://localhost:8080/api/appointments/<id>/` | Get Details of Individual Car Appointment History |
| PUT   |   `http://localhost:8080/api/appointments/<id>/` | Update Details of Individual Appointment |
| DELETE|   `http://localhost:8080/api/appointments/<id>/` | Delete Details of Individual Appointment |

- Create/POST Service Appointment - http://localhost:8080/api/appointments/
- JSON Input for Create or Update:
```
{
	"vin" : "aaaaaaaaaaaaaaaa",
	"customer" : "Daren",
	"date_time" : "2023-03-10 12:00",
	"reason": "bad everything",
	"technician" : "Bob"
}
```
- JSON Output for Create, Update, or Individual Details:
```
{
	"href": "/api/appointments/6/",
	"id": 6,
	"vin": "aaaaaaaaaaaaaaaa",
	"customer": "Daren",
	"date_time": "2023-03-10 12:00",
	"reason": "bad everything",
	"technician": {
		"id": 1,
		"name": "Bob",
		"employee_number": "1"
	},
	"is_vip": true,
	"is_complete": false
}
```
- List/Get Service Appointments - http://localhost:8080/api/appointments/
- JSON Output:
```
{
	"appointments": [
		{
			"href": "/api/appointments/4/",
			"id": 4,
			"vin": "88888888888888888",
			"customer": "fsakjbibaiv",
			"date_time": "2023-03-25T16:34:00+00:00",
			"reason": "fawgawga",
			"technician": {
				"id": 2,
				"name": "tech",
				"employee_number": "5"
			},
			"is_vip": true,
			"is_complete": true
		},
	]
}
```
