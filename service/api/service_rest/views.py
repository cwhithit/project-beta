from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
# Create your views here.
class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number"
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "color",
        "year"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer",
        "date_time",
        "reason",
        "technician",
        "is_vip",
        "is_complete",
    ]

    encoders = {
        "technician" : TechnicianEncoder(),
        "automobile" : AutomobileVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians" : technicians},
            encoder = TechnicianEncoder,
            safe = False,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder = TechnicianEncoder,
                safe = False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"Message" : "Technician does not exist."},
                status = 400,
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def detail_technicians(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id = pk)
            return JsonResponse(
                technician,
                encoder = TechnicianEncoder,
                safe = False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"Message" : "Technician does not exist."},
                status = 400
            )
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id = pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder = TechnicianEncoder,
                safe = False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"Message" : "Technician does not exist."})
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id = pk).update(**content)
        technician = Technician.objects.get(id = pk)
        return JsonResponse(
            technician,
            encoder = TechnicianEncoder,
            safe = False,
        )

@require_http_methods(["GET", "POST"])
def list_appointments(request, automobile_vo_id = None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            appointments = Appointment.objects.filter(automobile = automobile_vo_id)
        else:
            appointments = Appointment.objects.all()
            return JsonResponse(
                {"appointments" : appointments},
                encoder = AppointmentEncoder,
                safe = False,
            )
    else:
        content = json.loads(request.body)
        try:
            name = content["technician"]
            technician = Technician.objects.get(name = name)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"Message" : "Invalid Technician."},
                status = 400
            )
        if AutomobileVO.objects.filter(vin=content["vin"]).exists():
            content["is_vip"] = True
        else:
            content["is_vip"] = False
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder = AppointmentEncoder,
            safe = False
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def detail_appointments(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id = pk)
            return JsonResponse(
                appointment,
                encoder = AppointmentEncoder,
                safe = False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"Message": "Appointment Does Not Exist."},
                status = 400,
            )
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id = pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder = AppointmentEncoder,
                safe = False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"Message": "Appointment Does Not Exist."},
                status = 400,
            )
    else:
        try:
            appointment = Appointment.objects.get(id = pk)
            appointment.complete()
            return JsonResponse(
                appointment,
                encoder = AppointmentEncoder,
                safe = False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"Message": "Appointment Does Not Exist."},
                status = 400,
                )
