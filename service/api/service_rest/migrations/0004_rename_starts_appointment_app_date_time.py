# Generated by Django 4.0.3 on 2023-03-08 22:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0003_appointment_is_vip'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appointment',
            old_name='starts',
            new_name='app_date_time',
        ),
    ]
