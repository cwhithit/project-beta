from django.db import models
from django.urls import reverse
# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length = 200, unique = True, null = True)
    vin = models.CharField(max_length = 20, unique = True)
    color = models.CharField(max_length = 30)
    year = models.PositiveSmallIntegerField(null = True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length = 100)
    employee_number = models.CharField(max_length = 20, unique = True)

class Appointment(models.Model):
    vin = models.CharField(max_length = 17, null = True)
    customer = models.CharField(max_length = 100)
    date_time = models.DateTimeField(null = True)
    reason = models.CharField(max_length = 100)
    technician = models.ForeignKey(
        Technician,
        related_name = "appointments",
        on_delete = models.CASCADE,
        null = True
    )
    is_vip = models.BooleanField(default = False)
    is_complete = models.BooleanField(default = False)

    def get_api_url(self):
        return reverse("detail_appointments", kwargs = {"pk" : self.pk})

    def __str__(self):
        return self.vin

    def complete(self):
        self.is_complete = True
        self.save()

    class Meta:
        ordering = ("vin", "customer", "date_time", "reason", "technician")
